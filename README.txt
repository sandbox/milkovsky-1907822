
Entity reference unique

--------------------------------------------------------------------------------

Description:

This module provides special validation plugin "Validate unique entities" for 
Entity Reference field. 
An validation error will be shown if the entity is added twice or more in the
Entity Reference field.

--------------------------------------------------------------------------------

Usage:

1) go to your Entity reference field settings page
2) check "Validate unique entities" in "Additional behaviors" section
3) have fun